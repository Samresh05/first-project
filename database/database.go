package database

import (
	"context"
	"log"
	"path/filepath"

	"github.com/jackc/pgx/v4"
)

var conn *pgx.Conn

func DatabaseConnection() (*pgx.Conn, error) {

	absPath, _ := filepath.Abs("../app/database.crt")
	config, err := pgx.ParseConfig("postgres://isukacock:begooddogood@novel-coyote-3z9.gcp-us-east4.cockroachlabs.cloud:26257/" +
		"isu_db?sslmode=verify-full&sslrootcert=" + absPath)
	if err != nil {
		log.Fatal("error configuring the database: ", err)
	}
	config.TLSConfig.ServerName = "gowallet-twdwtabx5q-uc.a.run.app"

	// Connect to the "bank" database.
	conn, err := pgx.ConnectConfig(context.Background(), config)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}
	//	defer conn.Close(context.Background())

	return conn, err

}
