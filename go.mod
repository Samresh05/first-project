module main.go

go 1.14

require (
	github.com/cockroachdb/cockroach-go/v2 v2.0.8
	github.com/jackc/pgx/v4 v4.9.2
)
