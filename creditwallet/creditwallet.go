package creditwallet

import (
	"context"
	"fmt"
	"log"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"
	"main.go/database"
)

type Walletperform struct {
	id              int
	previousBalance float64
	currentBalance  float64
	walletid        int
	amount          float64
	is_sl           bool
	apiid           int
	apiwalletid     int
}

func (w *Walletperform) Setid(id int) {
	w.id = id
}
func (w Walletperform) getid() int {
	return w.id
}
func (w *Walletperform) SetpreviousBalance(previousBalance float64) {
	w.previousBalance = previousBalance
}
func (w Walletperform) getpreviousBalance() float64 {
	return w.previousBalance
}
func (w *Walletperform) SetcurrentBalance(currentBalance float64) {
	w.currentBalance = currentBalance
}
func (w Walletperform) getcurrentBalance() float64 {
	return w.currentBalance
}
func (w *Walletperform) Setwalletid(walletid int) {
	w.walletid = walletid
}
func (w Walletperform) getwalletid() int {
	return w.walletid
}
func (w *Walletperform) Setamount(amount float64) {
	w.amount = amount
}
func (w Walletperform) getamount() float64 {
	return w.amount
}
func (w *Walletperform) Setissl(is_sl bool) {
	w.is_sl = is_sl
}
func (w Walletperform) getissl() bool {
	return w.is_sl
}
func (w *Walletperform) Setapiid(apiid int) {
	w.apiid = apiid
}
func (w Walletperform) getapiid() int {
	return w.apiid
}
func (w *Walletperform) Setapiwalletid(apiwalletid int) {
	w.apiwalletid = apiwalletid
}
func (w Walletperform) getapiwalletid() int {
	return w.apiwalletid
}

func Creditwallet(walletid int, apiwalletid int, amount float64, uid int) int {
	conn, err := database.DatabaseConnection()

	fmt.Println(conn)
	fmt.Println("Wallet id in request", walletid)
	fmt.Println("Wallet id in request", apiwalletid)
	fmt.Println("Amount in request", amount)
	fmt.Println("User id in request", uid)

	//For Checking single layer or double layer
	walletda := Walletperform{}
	err = crdbpgx.ExecuteTx(context.Background(), conn, pgx.TxOptions{}, func(tx pgx.Tx) error {

		fmt.Println("User id in request in rollback", uid)

		apiid, issl, err := getsingleLayer(context.Background(), tx, uid)
		fmt.Println("single", issl)
		walletda.Setwalletid(walletid)
		walletda.Setapiwalletid(apiwalletid)
		walletda.Setamount(amount)
		walletda.Setissl(issl)
		fmt.Println(err)
		fmt.Println(apiid)
		return err
	})
	if err == nil {
		fmt.Println("Success")
		err = crdbpgx.ExecuteTx(context.Background(), conn, pgx.TxOptions{}, func(tx pgx.Tx) error {

			id, apiid, err := insertWalletledger(context.Background(), tx, walletda)
			walletda.Setid(id)
			walletda.Setapiid(apiid)

			return err
		})

		if err == nil {
			fmt.Println("Coming to wallet insert")
			err = crdbpgx.ExecuteTx(context.Background(), conn, pgx.TxOptions{}, func(tx pgx.Tx) error {

				err := updatewallet(context.Background(), tx, walletda)

				return err
			})

		} else {
			fmt.Println("Exception in insert ledger data ", err)
		}
	} else {
		fmt.Println("error is ", err)
	}

	return walletda.id
}

func getsingleLayer(ctx context.Context, tx pgx.Tx, uid int) (int, bool, error) {
	// Read the balance.
	fmt.Println("Enter in transactional", uid)
	var issl bool
	var apiid int
	if err := tx.QueryRow(context.Background(),
		"select api_userid,is_sl from user_api_data where userid=$1", uid).Scan(&apiid, &issl); err != nil {
		//Return wallet id is not available
		return -1, false, err
	}

	fmt.Println(apiid)
	fmt.Println("in fetch single layer", issl)
	return apiid, issl, nil
}

func insertWalletledger(ctx context.Context, tx pgx.Tx, walletda Walletperform) (int, int, error) {

	//issl := walletda.getissl
	issl := walletda.is_sl

	if issl {

		var id int
		var apiid int
		amount := walletda.amount
		from := walletda.walletid
		res, err := tx.Query(context.Background(),
			"INSERT INTO walletledgerdata(previousbalance,balance,currentbalance,walletid,status,type) VALUES (0,$1,0,$2,false,1) RETURNING id", amount, from)
		if err != nil {
			log.Fatal(err)
		}
		for res.Next() {
			if err := res.Scan(&id); err != nil {
				log.Fatal(err)
			}
			fmt.Printf("after insert debit %d\n", id)
		}
		return id, apiid, nil

	} else {

		var id int
		var apiid int
		amount := walletda.amount
		fromret := walletda.walletid
		fromapi := walletda.apiwalletid
		res, err := tx.Query(context.Background(),
			"INSERT INTO walletledgerdata(previousbalance,balance,currentbalance,walletid,status,type) VALUES (0,$1,0,$2,false,1) RETURNING id", amount, fromret)
		if err != nil {
			log.Fatal(err)
		}
		for res.Next() {
			if err := res.Scan(&id); err != nil {
				log.Fatal(err)
			}
			fmt.Printf("after insert debit %d\n", id)
		}
		res1, err := tx.Query(context.Background(),
			"INSERT INTO walletledgerdata(previousbalance,balance,currentbalance,walletid,status,type) VALUES (0,$1,0,$2,false,1) RETURNING id", amount, fromapi)
		if err != nil {
			log.Fatal(err)
		}
		for res1.Next() {
			if err := res.Scan(&apiid); err != nil {
				log.Fatal(err)
			}
			fmt.Printf("after insert debit %d\n", apiid)
		}
		return id, apiid, nil
	}

}

func updatewallet(ctx context.Context, tx pgx.Tx, walletda Walletperform) error {

	issl := walletda.is_sl
	if issl {

		var previousBalance float64
		walletid := walletda.walletid

		id := walletda.id

		amount := walletda.amount
		if err := tx.QueryRow(ctx,
			"SELECT balance FROM wallet WHERE id = $1", walletid).Scan(&previousBalance); err != nil {
			return err
		}
		if _, err := tx.Exec(ctx,
			"UPDATE wallet SET balance = balance + $1 WHERE id = $2", amount, walletid); err != nil {
			return err
		}
		var currentBalance float64
		currentBalance = previousBalance + amount
		if _, err := tx.Exec(context.Background(),
			"UPDATE walletledgerdata set previousbalance = $2 , currentbalance = $3 ,status= $4 where id = $1", id, previousBalance, currentBalance, true); err != nil {
			return err
		} else {
			fmt.Println("Wallet ledger updated")
		}
	} else {

		var previousBalance float64
		walletid := walletda.walletid

		id := walletda.id

		amount := walletda.amount
		if err := tx.QueryRow(ctx,
			"SELECT balance FROM wallet WHERE id = $1", walletid).Scan(&previousBalance); err != nil {
			return err
		}
		if _, err := tx.Exec(ctx,
			"UPDATE wallet SET balance = balance + $1 WHERE id = $2", amount, walletid); err != nil {
			return err
		}
		var currentBalance float64
		currentBalance = previousBalance + amount
		if _, err := tx.Exec(context.Background(),
			"UPDATE walletledgerdata set previousbalance = $2 , currentbalance = $3 ,status= $4 where id = $1", id, previousBalance, currentBalance, true); err != nil {
			return err
		} else {
			fmt.Println("Wallet ledger updated")
		}
		var apipreviousBalance float64
		apiwalletid := walletda.apiwalletid
		apiid := walletda.apiid
		if err := tx.QueryRow(ctx,
			"SELECT balance FROM wallet WHERE id = $1", apiwalletid).Scan(&apipreviousBalance); err != nil {
			return err
		}
		if _, err := tx.Exec(ctx,
			"UPDATE wallet SET balance = balance - $1 WHERE id = $2", amount, apiwalletid); err != nil {
			return err
		}
		var apicurrentBalance float64
		apicurrentBalance = apipreviousBalance - amount
		if _, err := tx.Exec(context.Background(),
			"UPDATE walletledgerdata set previousbalance = $2 , currentbalance = $3 ,status= $4 where id = $1", apiid, apipreviousBalance, apicurrentBalance, true); err != nil {
			return err
		} else {
			fmt.Println("Wallet ledger updated")
		}

	}

	return nil

}
