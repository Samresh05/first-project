package main

import (
	"encoding/json"
	"fmt"

	"net/http"

	"main.go/creditwallet"
	"main.go/debitwallet"
)

type Request struct {
	Userid      int
	Walletid    int
	Amount      float64
	Apiwalletid int
}

type Response struct {
	Txnid      int    `json:"txnId"`
	Status     int    `json:"status"`
	StatusDesc string `json:"statusDesc"`
}

func main() {

	http.HandleFunc("/", getapi)
	http.HandleFunc("/debitwallet", performdebit)
	http.HandleFunc("/creditwallet", performcredit)

	fmt.Println("wallet Prod server Started")
	http.ListenAndServe(":8080", nil)
}
func getapi(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Wallet Service Statred")
}

func performdebit(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {
		w.Header().Set("Content-Type", "application/json")
		//req := Request{}
		var req Request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			fmt.Println(err)
		}

		wid := req.Walletid
		amnt := req.Amount
		uid := req.Userid
		apiwallet := req.Apiwalletid
		fmt.Println("wallet id ", wid)
		fmt.Println("amount is :", amnt)

		id := debitwallet.Debitwallet(wid, apiwallet, amnt, uid)
		var apiResponse string
		var statuscode int = 0

		apiResponse = "Successfully Debited"
		response := Response{
			Txnid:      id,
			Status:     statuscode,
			StatusDesc: apiResponse}
		json.NewEncoder(w).Encode(response)

	} else {
		response := Response{
			Txnid:      0,
			Status:     -1,
			StatusDesc: "Invalid request method"}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusMethodNotAllowed)

		json.NewEncoder(w).Encode(response)

	}

}

func performcredit(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {
		w.Header().Set("Content-Type", "application/json")
		//req := Request{}
		var req Request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			fmt.Println(err)
		}

		wid := req.Walletid
		amnt := req.Amount
		uid := req.Userid
		apiwallet := req.Apiwalletid
		fmt.Println("wallet id ", wid)
		fmt.Println("amount is :", amnt)

		id := creditwallet.Creditwallet(wid, apiwallet, amnt, uid)
		var apiResponse string
		var statuscode int = 0

		apiResponse = "Successfully Credited"
		response := Response{
			Txnid:      id,
			Status:     statuscode,
			StatusDesc: apiResponse}
		json.NewEncoder(w).Encode(response)

	} else {
		response := Response{
			Txnid:      0,
			Status:     -1,
			StatusDesc: "Invalid request method"}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusMethodNotAllowed)

		json.NewEncoder(w).Encode(response)

	}

}
